import React, { useState } from "react";
import CardAuth from "../../components/cardAuth/card";
import Nav from "../../components/navauth/nav";
import StudentForm from "./components/studentForm";
import TeacherForm from "./components/teacherForm";

export default function Login() {
  const [item, setItem] = useState("student");

  return (
    <CardAuth title="Login">
      <Nav
        data={{
          text1: "Student",
          text2: "Teacher",
          item: item,
          setItem: setItem,
        }}
      />
      {item === "student" ? <StudentForm /> : <TeacherForm />}
    </CardAuth>
  );
}
