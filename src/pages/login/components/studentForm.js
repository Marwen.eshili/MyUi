import React from "react";
import { useFormik } from "formik";
import { Grid } from "@mui/material";
import Btn from "../../../components/button/btn";
export default function StudentForm() {
  const formik = useFormik({
    initialValues: {
      email: "",
      phone: "",
      password: "",
    },
    onSubmit: (values) => {
      console.log(values);
    },
  });
  return (
    <div className="form_login_card">
      <Grid container component="form" onSubmit={formik.handleSubmit}>
        <Grid item xs={12}>
          <label htmlFor="email">
            Email or Phone Number <span>*</span>
          </label>
        </Grid>
        <Grid item xs={12}>
          <input
            required
            placeholder="Enter your email or phone number"
            id="Email"
            name="email"
            type="email"
            onChange={formik.handleChange}
            value={formik.values.email}
          />
        </Grid>
        <Grid item xs={12}>
          <label htmlFor="password">
            Password <span>*</span>
          </label>
        </Grid>
        <Grid item xs={12}>
          <input
            placeholder="Enter your password"
            id="Password"
            name="password"
            type="password"
            onChange={formik.handleChange}
            value={formik.values.password}
          />
        </Grid>
        <div className="pwd_remember">
          <div className="s_left">
            <input type="checkbox" />
            <p>remember password</p>
          </div>
          <span>Forgot password</span>
        </div>

        <Btn
          type="submit"
          onClick={() => console.log("login here")}
          text="Log In"
        />
        <p
          style={{ textAlign: "center", color: "#707c9a" }}
          className="footer_card"
        >
          Don’t have an account? <span>Sign Up</span>
        </p>
      </Grid>
    </div>
  );
}
