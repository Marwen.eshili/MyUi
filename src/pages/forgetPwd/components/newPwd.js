import React, { useState } from "react";
import CardAuth from "../../../components/cardAuth/card";
import Btn from "../../../components/button/btn";

export default function NewPwd() {
  return (
    <CardAuth title="New Password">
      <Btn onClick={() => console.log("confirm pwd")} text="Confirm" />
    </CardAuth>
  );
}
