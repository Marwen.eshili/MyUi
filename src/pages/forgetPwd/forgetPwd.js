import React, { useState } from "react";
import CardAuth from "../../components/cardAuth/card";
import Btn from "../../components/button/btn";

export default function ForgetPwd() {
  return (
    <CardAuth title="Forget Password">
      <Btn onClick={() => console.log("forget pwd")} text="Recover" />
    </CardAuth>
  );
}
