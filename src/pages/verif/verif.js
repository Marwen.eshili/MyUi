import { Card, Grid } from "@mui/material";
import React from "react";
import CardAuth from "../../components/cardAuth/card";
import verif0 from "../../assets/svg/verif0.png";
import verif1 from "../../assets/svg/verif1.png";
import Btn from "../../components/button/btn";

export default function Verif() {
  const handleClick = () => {
    console.log("hello");
  };
  return (
    <CardAuth
      title=" Choose your verification method"
      subtitle="By choosing your verification method you will get a code to verified"
    >
      <Grid container spacing={2.5} className="sub_card">
        <Grid item xs={6}>
          <button>
            <img src={verif0}></img>Email
          </button>
        </Grid>
        <Grid item xs={6}>
          <button>
            <img src={verif1}></img>Phone Number
          </button>
        </Grid>
      </Grid>
      <Btn onClick={handleClick} text="Select" />
    </CardAuth>
  );
}
