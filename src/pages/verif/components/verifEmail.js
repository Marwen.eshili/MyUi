import { Card, Grid } from "@mui/material";
import React, { useState } from "react";
import CardAuth from "../../../components/cardAuth/card";
import Btn from "../../../components/button/btn";
import ReactCodeInput from "react-verification-code-input";

export default function VerifEmail() {
  const [value, setValue] = useState(0);
  const submit = (value) => {
    setValue(value);
  };

  const handleClick = () => {
    console.log("verif email", value);
  };
  return (
    <div className="verif_card">
      <CardAuth
        title="Verify your email address"
        subtitle="an code has been sended to your email: "
        email_number="karimbelhaj06@gmail.com"
      >
        <div className="form_verif_email">
          <ReactCodeInput
            onChange={submit}
            type="text"
            values={value}
            fields={6}
            className="input"
          />
        </div>
        <Btn onClick={handleClick} text="Confirm" />
        <p>Change The Email</p>
      </CardAuth>
    </div>
  );
}
