import { Card, Grid } from "@mui/material";
import React, { useState } from "react";
import CardAuth from "../../../components/cardAuth/card";
import Btn from "../../../components/button/btn";
import ReactCodeInput from "react-verification-code-input";

export default function VerifPhone() {
  const [value, setValue] = useState(0);
  const submit = (value) => {
    setValue(value);
  };

  const handleClick = () => {
    console.log("verif by phone", value);
  };
  return (
    <div className="verif_card">
      <CardAuth
        title="Verify your phone number"
        subtitle="an code has been sended to phone number: "
        email_number="+216 20 200 000"
      >
        <div className="form_verif_email">
          <ReactCodeInput
            onChange={submit}
            type="text"
            values={value}
            fields={6}
            className="input"
          />
        </div>
        <Btn onClick={handleClick} text="Select" />
        <p>Change phone number</p>
      </CardAuth>
    </div>
  );
}
