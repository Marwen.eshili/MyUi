import React, { useState } from "react";
import { Grid } from "@mui/material";
import CardAuth from "../../components/cardAuth/card";
import Nav from "../../components/navauth/nav";
import Btn from "../../components/button/btn";

export default function Signup() {
  const [item, setItem] = useState("student");
  return (
    <CardAuth title="Sign Up">
      <Nav
        data={{
          text1: "Student",
          text2: "Teacher",
          item: item,
          setItem: setItem,
        }}
      />
      {/* <Btn onClick={() => console.log("signup")} text="Sign Up" /> */}
    </CardAuth>
  );
}
