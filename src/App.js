import Signup from "./pages/signup/signup";
import Login from "./pages/login/login";
import Verif from "./pages/verif/verif";
import VerifEmail from "./pages/verif/components/verifEmail";
import VerifPhone from "./pages/verif/components/verifPhone";
import ForgetPwd from "./pages/forgetPwd/forgetPwd";
import NewPwd from "./pages/forgetPwd/components/newPwd";

function App() {
  return (
    <div>
      <Signup />
      <Login />
      <Verif />
      <VerifEmail />
      <VerifPhone />
      <ForgetPwd />
      <NewPwd />
    </div>
  );
}

export default App;
