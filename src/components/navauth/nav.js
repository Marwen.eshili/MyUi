import React from "react";
import userIcon from "../../assets/svg/user.svg";
export default function Nav({ data }) {
  const { text1, text2, setItem, item } = data;
  return (
    <div className="nav_items">
      <button
        type="button"
        className={item === "student" ? "clicked" : ""}
        onClick={() => setItem("student")}
      >
        <img className={item !== "student" ? "img" : ""} src={userIcon}></img>{" "}
        {text1}
      </button>
      <button
        className={item === "teacher" ? "clicked" : ""}
        onClick={() => setItem("teacher")}
      >
        <img className={item !== "teacher" ? "img" : ""} src={userIcon}></img>{" "}
        {text2}
      </button>
    </div>
  );
}
