import React from "react";
import { Card, Grid } from "@mui/material";
import Frame from "../../assets/svg/Frame.png";

export default function CardAuth(props) {
  const { title, subtitle, email_number } = props;
  return (
    <Grid
      container
      alignItems="center"
      justifyContent="center"
      className="back_card_section"
      spacing={0}
    >
      <Grid item xs={11} lg={5} className="card_section">
        <Card elevation={0} className="card_content">
          <img src={Frame} className="card_logo" />
          <div className="card_title">{title}</div>
          {subtitle && <div className="sub_title">{subtitle}</div>}
          {email_number && <div className="s-verif_item">{email_number}</div>}

          {props.children}
        </Card>
      </Grid>
    </Grid>
  );
}
