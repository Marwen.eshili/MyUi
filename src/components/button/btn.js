import React from "react";

export default function Btn({ onClick, text, type }) {
  return (
    <button type={type} className="btn btn_submit" onClick={() => onClick()}>
      {text}
    </button>
  );
}
